import os
import signal
import time
import logging

import sys
from threading import Thread, RLock
from client import * ;

from pyftpdlib.authorizers import DummyAuthorizer
##from pyftpdlib.authorizers import UnixAuthorizer
from pyftpdlib.handlers import FTPHandler, ThrottledDTPHandler
##from pyftpdlib.servers import FTPServer
from pyftpdlib.servers import MultiprocessFTPServer
from pyftpdlib.filesystems import UnixFilesystem

last_connection = []

verrou = RLock()

client = client() ;
ip = client.getIp() ;

print( "\n\n") ;
print( "------------------------------") ;
print( "Your Ip : " , ip ) ;
print( "------------------------------") ;
print( "\n\n") ;

class MyHandler(FTPHandler):

    def on_connect(self):
        print( "This user : ",self.remote_ip ," try to connect!" )
        
        if ( self.remote_ip in last_connection ) == False :
            
            response = input( "y / n : " )
            
            if response == "y" :
                last_connection.append( self.remote_ip)
                print("ok\n\n")
                print("This is list of custome connected\n")
                print( last_connection )
            else :
                self.close();
                print( "User deconnected" )
                print("This is list of custome connected\n")
                print( last_connection )

    def on_disconnect(self):
        # do something when client disconnects
        pass

    def on_login(self, username):
        # do something when user login
        print( "login")

    def on_logout(self, username):
        # do something when user logs out
        pass

    def on_file_sent(self, file):
        # do something when a file has been sent
        pass

    def on_file_received(self, file):
        # do something when a file has been received
        pass

    def on_incomplete_file_sent(self, file):
        # do something when a file is partially sent
        pass

    def on_incomplete_file_received(self, file):
        # remove partially uploaded files
        import os
        os.remove(file)

def main():
    # Instantiate a dummy authorizer for managing 'virtual' users
    # Instanciez un autorisateur factice pour gérer les utilisateurs `` virtuels ''
    authorizer = DummyAuthorizer()

    ##authorizer = UnixAuthorizer(rejected_users=["root"], require_valid_shell=True)

    # Define a new user having full r/w permissions and a read-only
    # anonymous user

    # Définir un nouvel utilisateur ayant des autorisations R / W complètes 
    # et un utilisateur anonyme en lecture seule
    #authorizer.add_user('user', '12345', '.', perm='elradfmwMT',msg_login = "Connexion start.",msg_quit = "Connexion close.")
    authorizer.add_anonymous(os.getcwd(),perm='elradfmwMT',msg_login = "Connexion start.",msg_quit = "Connexion close.")

    # limiter la vitesse des téléchargements et des 
    # uploads affectant le canal de données. 
    dtp_handler = ThrottledDTPHandler
    dtp_handler.read_limit = 3000720  # 30 Kb/sec (30 * 1024)
    dtp_handler.write_limit = 3000720  # 30 Kb/sec (30 * 1024)

    # Instantiate FTP handler class
    # Instancier la classe de gestionnaire FTP
    handler = MyHandler
    handler.authorizer = authorizer

    # have the ftp handler use the alternative dtp handler class
    # demandez au gestionnaire ftp d'utiliser la classe alternative du gestionnaire dtp
    handler.dtp_handler = dtp_handler

    # Modification du préfixe de ligne de journal
    handler.log_prefix = ' - [ %(username)s / %(remote_ip)s ]'

    # Define a customized banner (string returned when client connects)
    # Définir une bannière personnalisée (chaîne renvoyée lorsque le client se connecte)
    handler.banner = "pyftpdlib based ftpd ready."

    handler.timeout = 30000000

    # Specify a masquerade address and the range of ports to use for
    # passive connections.  Decomment in case you're behind a NAT.
    # handler.masquerade_address = '151.25.42.11'
    # handler.passive_ports = range(60000, 65535)

    # Vous pouvez activer la journalisation DEBUG pour 
    # observer les commandes et les réponses échangées
    # par le client et le serveur.
    # La journalisation DEBUG enregistre également les erreurs internes qui peuvent survenir lors des appels liés aux sockets tels que send()et recv().
    # Pour activer la journalisation DEBUG à partir du code, utilisez:
    logging.basicConfig(level=logging.DEBUG)

    # pyftpdlib utilise le module de journalisation pour gérer la journalisation.
    # Si vous ne configurez pas la journalisation, pyftpdlib écrira des journaux dans stderr.
    # Afin de configurer la journalisation, vous devez le faire avant d' appeler serve_forever ().
    # Exemple de journalisation dans un fichier:
    logging.basicConfig(filename='/var/log/pyftpd.log', level=logging.INFO)    

    # Spécifiez une adresse masquée et la plage de ports à utiliser pour la
    # connexions passives. Décommentez au cas où vous seriez derrière un NAT.
    # handler.masquerade_address = '151.25.42.11'
    # handler.passive_ports = range(60000, 65535)
    
    # Instantiate FTP server class and listen on 0.0.0.0:2121
    # Instanciez la classe de serveur FTP et écoutez sur 0.0.0.0:2121
    address = ('', 21)
    ##server = FTPServer(address, handler)
    server = MultiprocessFTPServer(address, handler)
    # set a limit for connections
    # définir une limite pour les connexions
    server.max_cons = 256
    server.max_cons_per_ip = 5

    # start ftp server
    # démarrer le serveur ftp
    server.serve_forever()


if __name__ == '__main__':
    main() ;